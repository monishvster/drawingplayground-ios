//Write Your Name Assignment

import UIKit
import PlaygroundSupport


class CustomView: UIView {
    override func draw(_ rect:CGRect){
        super.draw(rect)
        
        //Door wall
        let doorWall = UIBezierPath(roundedRect: CGRect(x:70, y:70, width:100, height:100), cornerRadius: 0.0)
        UIColor.green.set()
        UIColor.black.setStroke()
        doorWall.lineWidth  = 3.0
        doorWall.stroke()
        doorWall.fill()
        
        //Door
        let door = UIBezierPath(roundedRect: CGRect(x:97, y:110, width:40, height:60), cornerRadius: 0.0)
        UIColor.darkGray.set()
        UIColor.black.setStroke()
        door.lineWidth  = 3.0
        door.stroke()
        door.fill()
        
        //Roof Triangle
        let triangle = UIBezierPath()
        
        triangle.move(to: CGPoint(x: 120, y: 30))
        triangle.addLine(to: CGPoint(x: 170, y: 70))
        triangle.addLine(to: CGPoint(x: 70, y: 70))
        triangle.lineWidth  = 3.0
        triangle.close()
        UIColor.yellow.set()
        UIColor.black.setStroke()
                triangle.stroke()
        triangle.fill()
        
        //Roof Trapezoid
        let roof = UIBezierPath()
        
        roof.move(to: CGPoint(x: 120, y: 30))
        roof.addLine(to: CGPoint(x: 270, y: 30))
        roof.addLine(to: CGPoint(x: 310, y: 70))
        roof.addLine(to: CGPoint(x: 170, y: 70))
        UIColor.cyan.set()
        UIColor.black.setStroke()
        roof.lineWidth  = 3.0
        roof.stroke()
        roof.fill()
        
        //Side window Wall
        let windowWall = UIBezierPath(roundedRect: CGRect(x: 170, y: 70, width: 140, height: 100), cornerRadius: 0)
        UIColor.green.setFill()
        UIColor.black.setStroke()
        windowWall.lineWidth  = 3.0
        windowWall.stroke()
        windowWall.fill()
        
        //Window
        let window = UIBezierPath(roundedRect: CGRect(x: 200, y: 90, width: 70, height: 50), cornerRadius: 10)
        UIColor.darkGray.setFill()
        window.lineWidth  = 3.0
        window.fill()
        
        //Ventilator Circle
        let circle = UIBezierPath(ovalIn: CGRect(x: 110, y: 45, width: 20, height: 20))
        UIColor.red.setFill()
        circle.lineWidth  = 3.0
        circle.fill()
        
        //Path arc1
        let path1 = UIBezierPath(arcCenter: CGPoint(x:63, y: 170), radius: 75, startAngle: 2 * CGFloat.pi , endAngle: CGFloat.pi / 3, clockwise: true)
        path1.lineWidth  = 3.0
        path1.stroke()
        
        //Path arc2
        let path2 = UIBezierPath(arcCenter: CGPoint(x:22, y: 170), radius: 75, startAngle: 2 * CGFloat.pi , endAngle: CGFloat.pi / 3, clockwise: true)
        path2.lineWidth  = 3.0
        path2.stroke()
        
        
        // Writing Name
        //V
        let v = UIBezierPath()
        v.move(to: CGPoint(x: 100, y: 300))
        v.addLine(to: CGPoint(x: 120, y: 340))
        v.addLine(to: CGPoint(x: 140, y: 300))
        
        v.lineWidth = 5.0
        UIColor.yellow.setStroke()
        v.stroke()
        

        
        //e
        let e = UIBezierPath()
        
        e.addArc(withCenter: CGPoint(x: 160, y: 330), radius: 15, startAngle:CGFloat.pi / 7 , endAngle: 11 * CGFloat.pi / 6, clockwise: true)
        e.addLine(to: CGPoint(x: 145, y: 330))
        e.lineWidth = 5.0
        e.stroke()
        
        //r
        
        let r = UIBezierPath()
        
        r.move(to: CGPoint(x: 185, y: 312))
        r.addLine(to: CGPoint(x: 185, y: 347))
        r.addArc(withCenter: CGPoint(x: 195, y: 330), radius: 13, startAngle: 4 * CGFloat.pi / 3 , endAngle: 5 * CGFloat.pi / 3, clockwise: true)
        r.lineWidth = 5.0
        r.stroke()
        
        //m
        let m = UIBezierPath()
        
        m.move(to: CGPoint(x: 210, y: 312))
        m.addLine(to: CGPoint(x: 210, y: 347))
        m.addArc(withCenter: CGPoint(x: 220, y: 330), radius: 13, startAngle: 4 * CGFloat.pi / 3 , endAngle: 5 * CGFloat.pi / 3, clockwise: true)
        m.addLine(to: CGPoint(x: 225, y: 347))
        m.addArc(withCenter: CGPoint(x: 235, y: 330), radius: 13, startAngle: 4 * CGFloat.pi / 3 , endAngle: 5 * CGFloat.pi / 3, clockwise: true)
        m.addLine(to: CGPoint(x: 240, y: 347))
        m.lineWidth = 5.0
        m.stroke()
        
        //a
        let a = UIBezierPath()
    
        a.addArc(withCenter: CGPoint(x: 265, y: 330), radius: 15, startAngle:CGFloat.pi / 3 , endAngle: 5 * CGFloat.pi / 3, clockwise: true)
        a.addLine(to: CGPoint(x: 275, y: 348))
        a.lineWidth = 5.0
        a.stroke()
    }
    

}



let myFrame = CGRect(x: 0, y: 0, width: 400, height: 400)
let containerView = CustomView(frame: myFrame)
containerView.backgroundColor = UIColor.blue



PlaygroundPage.current.liveView = containerView
